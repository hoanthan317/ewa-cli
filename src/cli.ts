import arg from 'arg'
import inquirer from 'inquirer'
import pkinfo from 'pkginfo'
import create from './commands/create'

function parseArgumentsIntoOptions(rawArgs) {
    const args = arg(
        {
            '--name': String,
            '--db': String,
            '--default': Boolean,
            '--version': Boolean,
            '-n': '--name',
            '-d': '--default',
            '-v': '--version'
        },
        {
            argv: rawArgs.slice(2),
        }
    );
    return {
        name: args['--name'] || '',
        db: args['--db'] || '',
        default: args['--default'] || false,
        command: args._[0],
        version: args['--version'] || false
    };
}

async function checkCommand(options) {
    if (options.command) return;
    const answers = await inquirer.prompt({
        type: 'list',
        name: 'command',
        message: 'No command provided. Which command would you like to execute?',
        choices: ['create'],
        default: 'create'
    })
    options.command = answers.command
}

function showVersion() {
    pkinfo(module, 'version')
    console.log(module.exports['version'])
}

export async function cli(args) {
    let options = parseArgumentsIntoOptions(args)
    if (options.version) return showVersion()
    console.clear()
    await checkCommand(options)
    switch (options.command) {
        case 'create':
            await create(options)
            break;
        default:
            break;
    }
}