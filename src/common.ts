import fs from 'fs'
import path from 'path'

export const cwd = process.cwd()

export const dbDependencies = {
    mysql: ['mysql'],
    mariadb: ['mysql'],
    postgres: ['pg'],
    mssql: ['mssql']
}

export function getProjectDir(projectName) {
    return path.resolve(cwd, projectName || '')
}

export function createFoldersIfNotExist(filePath, startPath) {
    const pathComponents = filePath.split('/')

    pathComponents.forEach((dir, index) => {
        if (index === pathComponents.length - 1) return
        let absolutePath = path.resolve(startPath, dir)
        if (!fs.existsSync(absolutePath)) {
            fs.mkdirSync(absolutePath)
        }
        startPath = absolutePath
    })
}

export function isFile(path) {
    const pathComponents = path.split('/')
    const lastPath = pathComponents[pathComponents.length - 1]
    if (!lastPath.match(/\./g)) return false
    return true
}